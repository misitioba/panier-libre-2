const { setHeadlessWhen } = require('@codeceptjs/configure');

// turn on headless mode when running with HEADLESS=true environment variable
// HEADLESS=true npx codecept run
setHeadlessWhen(process.env.HEADLESS);

exports.config = {
    tests: 'tests/*_test.js',
    output: 'tests/output',
    helpers: {
        Puppeteer: {
            url: 'http://localhost:4000',
            show: false
        },
        REST: {
            endpoint: 'http://localhost:3000'
        },
        MyPuppeteer: {
            require: './tests/MyPuppeteer.js'
        }
    },
    include: {
        I: './steps_file.js'
    },
    bootstrap: null,
    mocha: {},
    name: 'pl2',
    plugins: {
        retryFailedStep: {
            enabled: true
        },
        screenshotOnFail: {
            enabled: true
        }
    }
}